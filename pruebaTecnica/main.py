from flask import Flask, request, jsonify
from collections import OrderedDict

app = Flask(__name__)

# Base de datos temporal para almacenar los productos
productos = []

# Ruta para obtener todos los productos
@app.route('/productos', methods=['GET'])
def get_productos():
    return jsonify([dict(p) for p in productos])

# Ruta para obtener un producto por su ID
@app.route('/productos/<int:producto_id>', methods=['GET'])
def get_producto(producto_id):
    for producto in productos:
        if producto['id'] == producto_id:
            return jsonify(dict(producto))
    return jsonify({'message': 'Producto no encontrado'}), 404

# Ruta para crear un nuevo producto
@app.route('/productos', methods=['POST'])
def crear_producto():
    data = request.get_json()
    # Verificar si ya existe un producto con el mismo nombre
    for producto in productos:
        if producto['nombre'] == data['nombre']:
            return jsonify({'message': 'El producto con este nombre ya existe'}), 400

    nuevo_producto = OrderedDict([
        ('id', len(productos) + 1),
        ('nombre', data['nombre']),
        ('precio', data['precio']),
        ('descripcion', data['descripcion'])
    ])
    productos.append(nuevo_producto)
    return jsonify(dict(nuevo_producto)), 201

# Ruta para actualizar un producto por su ID
@app.route('/productos/<int:producto_id>', methods=['PUT'])
def actualizar_producto(producto_id):
    for producto in productos:
        if producto['id'] == producto_id:
            data = request.get_json()
            producto['nombre'] = data.get('nombre', producto['nombre'])
            producto['precio'] = data.get('precio', producto['precio'])
            producto['descripcion'] = data.get('descripcion', producto['descripcion'])
            producto_ordenado = OrderedDict([
                ('id', producto['id']),
                ('nombre', producto['nombre']),
                ('descripcion', producto['descripcion']),
                ('precio', producto['precio'])
            ])
            return jsonify(dict(producto_ordenado))
    return jsonify({'message': 'Producto no encontrado'}), 404

# Ruta para eliminar un producto por su ID
@app.route('/productos/<int:producto_id>', methods=['DELETE'])
def eliminar_producto(producto_id):
    for producto in productos:
        if producto['id'] == producto_id:
            productos.remove(producto)
            return jsonify({'message': 'Producto eliminado'})
    return jsonify({'message': 'Producto no encontrado'}), 404

if __name__ == '__main__':
    app.run(debug=True)